import time
import socket

class ClientError(Exception):
	pass

class Client:
	def __init__(self, address, port, timeout = None):
		try:
			self.sock = socket.create_connection((address, port), timeout)
		except socket.error as err:
			raise ClientError("Cannot create connection", err)

	def _send_data(self, data):
		print(data)
		self.sock.sendall(data)
		rsp = self.sock.recv(1024)
		if rsp.startswith(b"error\n"):
			raise ClientError(rsp[len(b"error\n"):-len(b"\n\n")].decode("utf-8"))
		if rsp.startswith(b"ok\n"):
			return rsp[len(b"ok\n"):]
		else:
			raise ClientError("Protocol violation")

	def put(self, m_name, value, timestamp = None):
		if timestamp is None:
			timestamp = int(time.time())
		data = " ".join(("put", m_name, str(value), str(timestamp))) + "\n"
		self._send_data(data.encode("utf-8"))
	
	def get(self, m_name):
		data = " ".join(("get", m_name)) + "\n"
		rsp = self._send_data(data.encode("utf-8"))
		metrics = [m.decode("utf-8") for m in rsp.split(b"\n")]
		reply = {}
		try:
			for m in metrics:
				if m == "":
					break
				info = m.split(" ")
				if len(info) != 3:
					raise ClientError("Protocol violation")
				if reply.get(info[0]) is None:
					reply[info[0]] = []
				reply[info[0]].append((int(info[2]), float(info[1])))
		except ValueError:
			raise ClientError("Protocol violation")
		
		for _, value in reply.items():
			value.sort(key = lambda x: x[0])
		
		return reply
