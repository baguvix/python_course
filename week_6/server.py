import asyncio


class ClientServerProtocol(asyncio.Protocol):

	db = {}

	def connection_made(self, transport):
		self.transport = transport

	def data_received(self, data):
		print("Received: ", data)
		resp = self.process_data(data.decode())
		print("Response: ", resp.encode())
		self.transport.write(resp.encode())

	def process_data(self, data):
		try:
			query, payload = data.split(" ", 1)
			if query == "put":
				return self.process_put(payload)
			if query == "get":
				return self.process_get(payload)
			raise ValueError()
		except ValueError:
			return "error\nwrong command\n\n"

	def process_put(self, data):
		try:
			key, value, timestamp = data.strip().split()
			if key not in self.db:
				self.db[key] = []
			new = (float(value), int(timestamp))
			for i, val in enumerate(self.db[key]):
				if val[1] == new[1]:
					del self.db[key][i]
			self.db[key].append(new)
		except ValueError:
			return "error\nwrong command\n\n"
		return "ok\n\n"

	def process_get(self, data):
		data = data.strip()
		if data.count(" ") or data == "":
			return "error\nwrong command\n\n"
		resp = "ok\n"
		if data != "*":
			if self.db.get(data) is not None:
				for value in self.db[data]:
					resp = resp + f"{data} {value[0]} {value[1]}\n"
		else:
			for key, values in self.db.items():
				for value in values:
					resp = resp + f"{key} {value[0]} {value[1]}\n"
		return resp + "\n"

def run_server(host, port):
	loop = asyncio.get_event_loop()
	coro = loop.create_server(ClientServerProtocol, host, port)
	server = loop.run_until_complete(coro)
		
	try:
		loop.run_forever()
	except KeyboardInterrupt:
		pass

	server.close()
	loop.run_until_complete(server.wait_closed())
	loop.close()

if __name__ == "__main__":
	run_server("127.0.0.1", 8888)
