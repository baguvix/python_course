class Value:
	def __init__(self):
		self.val = None

	def __get__(self, obj, obj_type):
		return self.val

	def __set__(self, obj, value):
		self.val = value * (1 - obj.commission)

class Account:
	
	amount = Value()
	
	def __init__(self, commission):
		self.commission = commission

def main():
	my = Account(0.1)
	my.amount = 100
	print(my.amount)
	my.amount = 500
	print(my.amount)

if __name__ == "__main__":
	main()
