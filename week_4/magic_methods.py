import tempfile
import os

class File:
	
	def __init__(self, filepath):
		if not os.path.exists(filepath):
    			with open(filepath, 'w'): pass
		self.filepath = filepath

	def read(self):
		with open(self.filepath, 'r') as f:
			return f.read()

	def write(self, text):
		with open(self.filepath, 'w') as f:
			f.write(text)

	def __add__(self, obj):
		with tempfile.NamedTemporaryFile(mode = 'w', delete = False) as f:
			new = File(f.name)
		new.write(self.read() + obj.read())
		return new

	def __str__(self):
		return os.path.abspath(self.filepath)

	def __iter__(self):
		self.iter_file = open(self.filepath, 'r')
		return self.iter_file

	def __next__(self):
		return self.iter_file.__next__()

def main():
	file_obj = File(sys.argv[1])
	print(file_obj.read())
	file_obj.write(sys.argv[2])
	print(file_obj.read())

	path_to_file = "ololo"
	file_obj_1 = File(path_to_file + '_1')
	file_obj_2 = File(path_to_file + '_2')
	file_obj_1.write('line 1\n')
	file_obj_2.write('line 2\n')
	new_file_obj = file_obj_1 + file_obj_2
	print(id(file_obj_1), id(file_obj_2), id(new_file_obj), sep = '\n')
	print(new_file_obj)
	for line in new_file_obj:
		print(ascii(line))

if __name__ == "__main__":
	import sys
	main()
