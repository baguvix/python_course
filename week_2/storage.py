import argparse
import os
import tempfile
import json

def add_value(storage, new_key, value):
    if storage.get(new_key) is None:
        storage[new_key] = [value]
    else:
        storage[new_key].append(value)

def get_value(storage, requested_key):
	for key, value in storage.items():
		if key == requested_key:
			return value
	return None

def main():
	parser = argparse.ArgumentParser(description="Tiny <key:value> storage for your lil secrets ~o.o~")
	parser.add_argument("--key")
	parser.add_argument("--value")
	args = parser.parse_args()

	if args.key is None:
		exit()
        
	storage_path = os.path.join(tempfile.gettempdir(), "storage.data")
	
	open_mode = "w"
	if os.path.exists(storage_path):
		open_mode = "r+"

	with open(storage_path, open_mode) as f:
		storage = {}
		if os.path.getsize(storage_path) != 0:
			storage = json.load(f)
		if args.value is None:
			values = get_value(storage, args.key)
			if values is not None:
				print(', '.join(values))
		else:
			add_value(storage, args.key, args.value)
			f.seek(0)
			json.dump(storage, f)

if __name__ == "__main__":
    main()
