
def	nums_to_str(nums):
	return list(map(str, nums))

nums = [3123, 4424, 53213, 6995]
print(nums)
print(type(nums))
print(type(nums[0]))

strs = nums_to_str(nums)
print(strs)
print(type(strs))
print(type(strs[0]))
