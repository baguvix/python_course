import sys

a, b, c = float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3])

D = (b**2 - 4 * a * c)**0.5
print (int((-b - D)/(2 * a)))
print (int((-b + D)/(2 * a)))
