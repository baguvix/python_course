import  csv
import  os

class CarBase:
    def __init__(self, brand, photo_file_name, carrying):
        self.brand = brand
        self.photo_file_name = photo_file_name
        self.carrying = float(carrying)

    def get_photo_file_ext(self):
        _, ext = os.path.splitext(self.photo_file_name)
        return ext

class Car(CarBase):
    def __init__(self, brand, photo_file_name, carrying, passenger_seats_count):
        super().__init__(brand, photo_file_name, carrying)
        self.passenger_seats_count = int(passenger_seats_count)
        self.car_type = "car"

    def __repr__(self):
        return f"Light {self.car_type}"

class Truck(CarBase):
    def __init__(self, brand, photo_file_name, carrying, body_whl):
        super().__init__(brand, photo_file_name, carrying)
        self.car_type = "truck"
        dim = body_whl.split('x')
        try:
            if (len(dim) != 3):
                raise ValueError(f"Incorrect dimensions format: {body_whl}")
            self.body_length = float(dim[0])
            self.body_width = float(dim[1])
            self.body_height = float(dim[2])
        except (ValueError, OverflowError) as err:
            self.body_length = 0.0
            self.body_width = 0.0
            self.body_height = 0.0

    def __repr__(self):
        return f"Cyber-{self.car_type}"

    def get_body_volume(self):
        return  self.body_length *  \
                self.body_width *   \
                self.body_height


class SpecMachine(CarBase):
    def __init__(self, brand, photo_file_name, carrying, extra):
        super().__init__(brand, photo_file_name, carrying)
        self.extra = extra
        self.car_type = "spec_machine"

    def __repr__():
        return f"{self.car_type}"

acceptible_photo_file_types = (".jpg", ".jpeg", ".png", ".gif")


def create_car(row):
    if row["passenger_seats_count"] == "":
        raise ValueError("Empty passenger_seats_count field")
    return Car(row["brand"], row["photo_file_name"], \
               row["carrying"], row["passenger_seats_count"])

def create_truck(row):
    return Truck(row["brand"], row["photo_file_name"], \
                 row["carrying"], row["body_whl"])

def create_spec(row):
    if row["extra"] == "":
        raise ValueError("Empty extra field")
    return SpecMachine(row["brand"], row["photo_file_name"], \
                       row["carrying"], row["extra"])

def create_car_obj(row):
    if row["brand"] == "":
        raise ValueError("Empty brand field")
    _, ext = os.path.splitext(row["photo_file_name"])
    if ext not in acceptible_photo_file_types:
        raise ValueError(f"Bad photo file type: {0}".format(ext))

    if row["car_type"] == "car":
        return create_car(row)
    elif row["car_type"] == "truck":
        return create_truck(row)
    elif row["car_type"] == "spec_machine":
        return create_spec(row)
    else:
        raise ValueError("Bad car type: {0}".format(row["car_type"]))

def get_car_list(csv_filename):
    car_list = []
    with open(csv_filename) as csv_fd:
        reader = csv.DictReader(csv_fd, delimiter=';')
        for row in reader:
            try:
                car_list.append(create_car_obj(row))
            except ValueError:
                pass
    return car_list

def main():
    carl = get_car_list(sys.argv[1])
    print(carl)
    for car in carl:
        print (car)
        if isinstance(car, Truck):
            print(f"{car.brand} volume is {car.get_body_volume()}")

if __name__ == "__main__":
    import sys
    main()
