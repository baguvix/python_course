class FileReader:
    """Class for line by line file reading."""

    def __init__(self, filename):
        self.filename = filename
        self.fileobj = None

    def _create_fileobj(self):
        if self.fileobj is None:
            try:
                self.fileobj = open(self.filename, "r")
            except FileNotFoundError as err:
                raise

    def read(self):
        try:
            self._create_fileobj()
            return self.fileobj.read()
        except FileNotFoundError:
            return str()

def main():
    reader = FileReader(sys.argv[1])
    ctx = reader.read()
    print(ctx)

if __name__ == "__main__":
    import  sys
    main()
